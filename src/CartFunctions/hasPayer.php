<?php

namespace Improoving\CartFunctions;

/**
 * Trait for deletable resources. Adds a `delete()` method to the class.
 *
 * This trait should only be applied to classes that derive from ImproovingObject.
 */
trait hasPayer
{
    private static $payer;

    public static function getPayer(){
        return self::$payer;
    }

    public static function payerIsSet(){
        return !empty(self::$payer);
    }

    public static function removePayer(){
        self::$payer = null;
    }

    public function useNewPayer($first_name, $last_name, $email, $phone, $password){
        self::removePayer();
        self::_setPayer($first_name, $last_name, $email, $phone);
        self::$payer['password'] = $password;
        self::$payer['password_confirmation'] = $password;
    }
    public function useRegisteredPayer($person_id, $first_name, $last_name, $email, $phone){
        self::removePayer();
        self::_setPayer($first_name, $last_name, $email, $phone);
        self::$payer['person_id'] = $person_id;
    }

    private static function _setPayer($first_name, $last_name, $email, $phone){
        self::$payer = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'phone' => $phone
        );
    }

}