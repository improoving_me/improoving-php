<?php

namespace Improoving\CartFunctions;

/**
 * Trait for deletable resources. Adds a `delete()` method to the class.
 *
 * This trait should only be applied to classes that derive from ImproovingObject.
 */
trait hasSchool
{
    private static $school;

    public static function getSchool(){
        return self::$school;
    }

    public static function schoolIsSet(){
        return !empty(self::$school);
    }

    public static function setSchool(int $school_id){
        self::$school = $school_id;
    }

}