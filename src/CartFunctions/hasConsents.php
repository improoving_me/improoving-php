<?php

namespace Improoving\CartFunctions;

use Improoving\Consent;

/**
 * Trait for deletable resources. Adds a `delete()` method to the class.
 *
 * This trait should only be applied to classes that derive from ImproovingObject.
 */
trait hasConsents
{
    private static $consents;

    public static function getConsents(){
        return self::$consents;
    }

    public function giveConsent($consent_type){
        if(!in_array($consent_type, array_keys(self::$consents)))
            throw new \InvalidArgumentException('Consent does not exist');

        self::$consents[$consent_type] = true;
    }
    public function removeConsent($consent_type){
        if(!in_array($consent_type, array_keys(self::$consents)))
            throw new \InvalidArgumentException('Consent does not exist');

        self::$consents[$consent_type] = false;
    }

    private function _buildConsentsArray(){
        if(!self::schoolIsSet())
            throw new \InvalidArgumentException('You must specify a School before building the consents array.  HINT: use \Improoving\Cart::setSchool().');

        $consents = array(
            'schools' => array(),
            'platform' => array(
                Consent::$default_consent => true
            )
        );
        foreach(self::$consents as $consent => $value)
            $consents['schools'][self::$school][$consent] = $value;

        return $consents;
    }
}