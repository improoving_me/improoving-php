<?php

namespace Improoving;

use Improoving\ApiOperations\All;

class District extends ApiResource
{
    const OBJECT_NAME = 'district';

    public $permanent_attributes = array('name', 'lat', 'lng', 'custom_query', 'approved');

    use All;

    private static $allUrl = '/districts/export';

}
