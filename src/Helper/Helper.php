<?php

namespace Improoving\Helper;

abstract class Helper
{
    private static $isMbstringAvailable = null;
    private static $isHashEqualsAvailable = null;

    /**
     * Whether the provided array (or other) is a list rather than a dictionary.
     * A list is defined as an array for which all the keys are integers.
     * Empty arrays are considered to be lists.
     *
     * @param array|mixed $array
     * @return boolean true if the given object is a list.
     */
    public static function isList($array)
    {
        if (!is_array($array)) {
            return false;
        }
        if ($array === []) {
            return true;
        }
        $keys = array_keys($array);
        if ($keys !== array_filter($keys,'is_int')) {
            return false;
        }
        return true;
    }

    /**
     * Recursively goes through an array of parameters. If a parameter is an instance of
     * ApiResource, then it is replaced by the resource's ID.
     * Also clears out null values.
     *
     * @param mixed $h
     * @return mixed
     */
    public static function objectsToIds($h)
    {
        if ($h instanceof \Improoving\ApiResource) {
            return $h->id;
        } elseif (static::isList($h)) {
            $results = [];
            foreach ($h as $v) {
                array_push($results, static::objectsToIds($v));
            }
            return $results;
        } elseif (is_array($h)) {
            $results = [];
            foreach ($h as $k => $v) {
                if (is_null($v)) {
                    continue;
                }
                $results[$k] = static::objectsToIds($v);
            }
            return $results;
        } else {
            return $h;
        }
    }

    /**
     * @param string|mixed $value A string to UTF8-encode.
     *
     * @return string|mixed The UTF8-encoded string, or the object passed in if
     *    it wasn't a string.
     */
    public static function utf8($value)
    {
        if (self::$isMbstringAvailable === null) {
            self::$isMbstringAvailable = function_exists('mb_detect_encoding');

            if (!self::$isMbstringAvailable) {
                trigger_error("It looks like the mbstring extension is not enabled. " .
                    "UTF-8 strings will not properly be encoded. Ask your system " .
                    "administrator to enable the mbstring extension, or write to " .
                    "support@stripe.com if you have any questions.", E_USER_WARNING);
            }
        }

        if (is_string($value) && self::$isMbstringAvailable && mb_detect_encoding($value, "UTF-8", true) != "UTF-8") {
            return utf8_encode($value);
        } else {
            return $value;
        }
    }

    /**
     * @param array $params
     *
     * @return string
     */
    public static function encodeParameters($params)
    {
        $flattenedParams = self::flattenParams($params);
        $pieces = [];
        foreach ($flattenedParams as $param) {
            list($k, $v) = $param;
            array_push($pieces, self::urlEncode($k) . '=' . self::urlEncode($v));
        }
        return implode('&', $pieces);
    }

    /**
     * @param array $params
     * @param string|null $parentKey
     *
     * @return array
     */
    public static function flattenParams($params, $parentKey = null)
    {
        $result = [];

        foreach ($params as $key => $value) {
            $calculatedKey = $parentKey ? "{$parentKey}[{$key}]" : $key;

            if (self::isList($value)) {
                $result = array_merge($result, self::flattenParamsList($value, $calculatedKey));
            } elseif (is_array($value)) {
                $result = array_merge($result, self::flattenParams($value, $calculatedKey));
            } else {
                array_push($result, [$calculatedKey, $value]);
            }
        }

        return $result;
    }

    /**
     * @param array $value
     * @param string $calculatedKey
     *
     * @return array
     */
    public static function flattenParamsList($value, $calculatedKey)
    {
        $result = [];

        foreach ($value as $i => $elem) {
            if (self::isList($elem)) {
                $result = array_merge($result, self::flattenParamsList($elem, $calculatedKey));
            } elseif (is_array($elem)) {
                $result = array_merge($result, self::flattenParams($elem, "{$calculatedKey}[{$i}]"));
            } else {
                array_push($result, ["{$calculatedKey}[{$i}]", $elem]);
            }
        }

        return $result;
    }

    /**
     * @param string $key A string to URL-encode.
     *
     * @return string The URL-encoded string.
     */
    public static function urlEncode($key)
    {
        $s = urlencode($key);

        // Don't use strict form encoding by changing the square bracket control
        // characters back to their literals. This is fine by the server, and
        // makes these parameter strings easier to read.
        $s = str_replace('%5B', '[', $s);
        $s = str_replace('%5D', ']', $s);

        return $s;
    }

    public static function normalizeId($id)
    {
        if (is_array($id)) {
            $params = $id;
            $id = $params['id'];
            unset($params['id']);
        } else {
            $params = [];
        }
        return [$id, $params];
    }

    public static function convertToImproovingObject($response, $object_name = null){
        $types = [
            // business objects
            \Improoving\Account::OBJECT_NAME => 'Improoving\\Account',
            \Improoving\Association::OBJECT_NAME => 'Improoving\\Association',
            \Improoving\Category::OBJECT_NAME => 'Improoving\\Category',
            \Improoving\Consent::OBJECT_NAME => 'Improoving\\Consent',
            \Improoving\District::OBJECT_NAME => 'Improoving\\District',
            \Improoving\Language::OBJECT_NAME => 'Improoving\\Language',
            \Improoving\LevelGroup::OBJECT_NAME => 'Improoving\\LevelGroup',
            \Improoving\Location::OBJECT_NAME => 'Improoving\\Location',
            \Improoving\Packet::OBJECT_NAME => 'Improoving\\Packet',
            \Improoving\Person::OBJECT_NAME => 'Improoving\\Person',
            \Improoving\School::OBJECT_NAME => 'Improoving\\School',
            \Improoving\Slot::OBJECT_NAME => 'Improoving\\Slot',
            \Improoving\Specialization::OBJECT_NAME => 'Improoving\\Specialization',
            \Improoving\User::OBJECT_NAME => 'Improoving\\User',

            // relations and renames
            'associations' => 'Improoving\\Association',
            'consents' => 'Improoving\\Consent',
            'favourite_category' => 'Improoving\\Category',
            'languages' => 'Improoving\\Language',
            'packet_prices' => 'Improoving\\Person',
            'payers' => 'Improoving\\Person',
            'pupils' => 'Improoving\\Person',
            'specializations' => 'Improoving\\Specialization',
            'valid_consents' => 'Improoving\\Consent',
            'variations' => 'Improoving\\Packet',
        ];
        if (self::isList($response)) {
            $mapped = [];
            foreach ($response as $i) {
                array_push($mapped, self::convertToImproovingObject($i, $object_name));
            }
            return $mapped;
        } elseif (is_array($response)) {
            if (!empty($object_name) && is_string($object_name) && isset($types[$object_name])) {
                $class = $types[$object_name];
            } else {
                $class = 'Improoving\\ImproovingObject';
            }
            return $class::constructFrom($response);
        } else {
            return $response;
        }
    }

    public static function isValidDate($date){
        return preg_match('/^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$/', $date);
    }
    public static function isValidTime($time){
        return preg_match('/^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', $time);
    }
}
