<?php

namespace Improoving;

use Improoving\ApiOperations\All;

class Category extends ApiResource
{
    const OBJECT_NAME = 'category';

    public $permanent_attributes = array('name', 'specialization', 'limited_to_school', 'icon');

    use All;

    private static $allUrl = '/categories';

}
