<?php

namespace Improoving;

class Specialization extends ApiResource
{
    const OBJECT_NAME = 'specialization';

    public $permanent_attributes = array('association_id', 'category_school_id', 'person_id', 'category_id');
    public $related_models = array('association', 'category', 'person');

}