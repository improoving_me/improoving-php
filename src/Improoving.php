<?php

namespace Improoving;

/**
 * Class Improoving
 *
 * @package Improoving
 */
class Improoving
{
    // @var string The Improoving API key to be used for requests.
    public static $apiKey;

    // @var string The base URL for the Improoving API.
    public static $apiBase = 'https://improoving.me/api';
    public static $apiBaseTest = 'https://test.improoving.me/api';

    // @var string Defines if we are in test environment.
    public static $isTestEnvironment = false;

    // @var string|null The version of the Stripe API to use for requests.
    public static $apiVersion = null;

    // @var array The application's information (name, version, URL)
    public static $appInfo = null;

    // @var Helper\LoggerInterface|null The logger to which the library will
    //   produce messages.
    public static $logger = null;

    // @var int Maximum number of request retries
    public static $maxNetworkRetries = 0;

    // @var float Maximum delay between retries, in seconds
    private static $maxNetworkRetryDelay = 2.0;

    // @var float Initial delay between retries, in seconds
    private static $initialNetworkRetryDelay = 0.5;

    const VERSION = '0.1.1';

    public static $bearer_token;

    public static function setBearerToken($bearer_token){
        self::$bearer_token = $bearer_token;
    }

    public static function getBearerToken(){
        return self::$bearer_token;
    }

    public static function bearerTokenIsSet(){
        return !empty(self::$bearer_token);
    }

    public static function getRequestHeaders(){
        // Public because of testing
        $headers = array();
        if(self::bearerTokenIsSet()){
            $headers['Authorization'] = 'Bearer: '.self::getBearerToken();
            $headers['BearerToken'] = self::getBearerToken();
        }

        return $headers;
    }

    /**
     * @return string The API key used for requests.
     */
    public static function getApiKey()
    {
        return self::$apiKey;
    }

    /**
     * Sets the API key to be used for requests.
     *
     * @param string $apiKey
     */
    public static function setApiKey($apiKey)
    {
        self::$apiKey = $apiKey;
    }

    /**
     * Sets if we are in test environment
     *
     * @param bool $isTestEnvironment
     */
    public static function setTestEnvironment(bool $isTestEnvironment)
    {
        self::$isTestEnvironment = $isTestEnvironment;
    }

    /**
     * @return string The url needed to contact the API
     */
    public static function getApiUrl()
    {
        return self::$isTestEnvironment ? self::$apiBaseTest : self::$apiBase;
    }

    /**
     * @return Helper\LoggerInterface The logger to which the library will
     *   produce messages.
     */
    public static function getLogger()
    {
        if (self::$logger == null) {
            return new Helper\DefaultLogger();
        }
        return self::$logger;
    }

    /**
     * @param Helper\LoggerInterface $logger The logger to which the library
     *   will produce messages.
     */
    public static function setLogger($logger)
    {
        self::$logger = $logger;
    }

    /**
     * @return string The API version used for requests. null if we're using the
     *    latest version.
     */
    public static function getApiVersion()
    {
        return self::$apiVersion;
    }

    /**
     * @param string $apiVersion The API version to use for requests.
     */
    public static function setApiVersion($apiVersion)
    {
        self::$apiVersion = $apiVersion;
    }

    /**
     * @return array | null The application's information
     */
    public static function getAppInfo()
    {
        return self::$appInfo;
    }

    /**
     * @param string $appName The application's name
     * @param string $appVersion The application's version
     * @param string $appUrl The application's URL
     */
    public static function setAppInfo($appName, $appVersion = null, $appUrl = null, $appPartnerId = null)
    {
        self::$appInfo = self::$appInfo ?: [];
        self::$appInfo['name'] = $appName;
        self::$appInfo['partner_id'] = $appPartnerId;
        self::$appInfo['url'] = $appUrl;
        self::$appInfo['version'] = $appVersion;
    }


    /**
     * @return int Maximum number of request retries
     */
    public static function getMaxNetworkRetries()
    {
        return self::$maxNetworkRetries;
    }

    /**
     * @param int $maxNetworkRetries Maximum number of request retries
     */
    public static function setMaxNetworkRetries($maxNetworkRetries)
    {
        self::$maxNetworkRetries = $maxNetworkRetries;
    }

    /**
     * @return float Maximum delay between retries, in seconds
     */
    public static function getMaxNetworkRetryDelay()
    {
        return self::$maxNetworkRetryDelay;
    }

    /**
     * @return float Initial delay between retries, in seconds
     */
    public static function getInitialNetworkRetryDelay()
    {
        return self::$initialNetworkRetryDelay;
    }
}
