<?php

namespace Improoving;

class Service extends ApiResource
{
    const OBJECT_NAME = 'service';

    public $permanent_attributes = array('school_id');
    public $related_models = array('school');

}