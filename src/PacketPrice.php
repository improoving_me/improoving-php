<?php

namespace Improoving;

class PacketPrice extends ApiResource
{
    const OBJECT_NAME = 'packetprice';

    public $permanent_attributes = array('packet_id');
}