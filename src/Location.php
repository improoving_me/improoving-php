<?php

namespace Improoving;

use Improoving\ApiOperations\All;

class Location extends ApiResource
{
    const OBJECT_NAME = 'location';

    public $permanent_attributes = array('name', 'lat', 'lng');

    use All;

    private static $allUrl = '/locations';

}
