<?php

namespace Improoving;

class Association extends ApiResource
{
    const OBJECT_NAME = 'association';

    public $permanent_attributes = array('person_id', 'school_id', 'circle_id');
    public $related_models = array('favourite_category', 'person', 'school', 'specializations');

    public function getPurchasablePlanner(array $params = array()){
        if(!self::bearerTokenIsSet())
            throw new \InvalidArgumentException('You must set a Bearer Token for this call.  HINT: use \Improoving::setBearerToken().');

        return $this->_getPlanner('teachers', $params);
    }

    public function getPlanner(array $params = array()){
        return $this->_getPlanner('teachers', $params);
    }

    private function _getPlanner($association_id, array $params = array()){
        $params['OBJECT_NAME'] = Slot::OBJECT_NAME;

        $requestor = new ApiRequestor();

        return $requestor->request(
            'get',
            join('/', array(self::getApiUrl().'/purchasableplanner', $association_id)),
            $params,
            self::getRequestHeaders()
        );
    }


}