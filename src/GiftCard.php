<?php

namespace Improoving;

class GiftCard extends ApiResource
{
    const OBJECT_NAME = 'giftcard';

    public $permanent_attributes = array('school_id');
    public $related_models = array('school');

}