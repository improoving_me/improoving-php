<?php

namespace Improoving;

/**
 * Class ApiResource
 *
 * @package Improoving
 */
abstract class ApiResource extends ImproovingObject
{
    use ApiOperations\Request;

}
