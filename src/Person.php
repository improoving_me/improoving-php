<?php

namespace Improoving;

class Person extends ApiResource
{
    const OBJECT_NAME = 'person';

    public $permanent_attributes = array('account_id', 'account_type');
    public $related_models = array('associations', 'account', 'consents', 'valid_consents', 'languages');

    private static $consents;

    public function giveConsent($consent_type){
        if(!in_array($consent_type, array_keys(Consent::$possible_consents_customers)))
            throw new \InvalidArgumentException('Consent does not exist');

        self::$consents[$consent_type] = true;
    }
    public function removeConsent($consent_type){
        if(!in_array($consent_type, array_keys(Consent::$possible_consents_customers)))
            throw new \InvalidArgumentException('Consent does not exist');

        self::$consents[$consent_type] = false;
    }
    public function hasConsent($consent_type){
        if(!in_array($consent_type, array_keys(Consent::$possible_consents_customers)))
            throw new \InvalidArgumentException('Consent does not exist');

        return self::$consents[$consent_type];
    }

    public function consentsArrayForInput(){
        $array = array();
        foreach(self::$consents as $key => $value)
            if(!empty($value))
                $array[$key] = $value;

        return $array;
    }

}