<?php

namespace Improoving;

use Improoving\ApiOperations\All;

class Consent extends ApiResource
{
    const OBJECT_NAME = 'consent';

    public $permanent_attributes = array('consent_type', 'is_default', 'is_negative', 'applicability', 'person_id', 'school_id');

    use All;

    private static $allUrl = '/consents';

    public static $default_consent = 'data_collection';
    public static $picture_consent = 'photo_collection';
    public static $negative_consents = ['data_treatment'];
    public static $never_expiring = ['reviews_collection'];
    public static $possible_consents_platform = [
        'data_collection',
        'data_treatment', // Negative consent
        'photo_collection',
        'communications_related_to_purchases',
        'communications_related_to_platform',
        'communications_related_to_partners'
    ];
    public static $additional_consents_platform_for_teachers = [
        'reviews_collection',
    ];
    public static $additional_consents_platform_for_staff = [];
    public static $additional_consents_platform_for_resellers = [];
    public static $possible_consents_customers = [
        'data_collection',
        'data_treatment', // Negative consent
        'photo_collection',
        'communications_related_to_purchases',
        'communications_related_to_school',
        'communications_related_to_partners'
    ];
    public static $possible_consents_teachers = [
        'data_collection',
        'data_treatment', // Negative consent
        'photo_collection',
    ];
    public static $possible_consents_staff = [
        'data_collection',
        'data_treatment', // Negative consent
        'photo_collection',
    ];
    public static $possible_consents_resellers = [
        'data_collection',
        'data_treatment', // Negative consent
        'photo_collection',
    ];


}
