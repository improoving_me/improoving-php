<?php

namespace Improoving\HttpClient;

use GuzzleHttp;
use Improoving\Error;

class GuzzleClient implements ClientInterface
{

    private static $instance;

    public static function instance()
    {
        if (!self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function request($method, $absUrl, $headers, $params, $hasFile = false)
    {
        $requestor = new GuzzleHttp\Client();

        $method = strtolower($method);
        try{
            switch($method){
                case 'get':
                    $response = $requestor->get($absUrl, [
                        'headers' => $headers,
                        'query' => $params
                    ]);
                    break;
                case 'post':
                    $response = $requestor->post($absUrl, [
                        'headers' => $headers,
                        'form_params' => $params,
                    ]);
                    break;
                default:
                    throw new Error\Api("Unrecognized method $method");
            }

            return $this->decodeResponse($response);
        }
        catch(GuzzleHttp\Exception\RequestException $e){
            throw new Error\HttpClient(
                $e->getMessage(),
                $e->getCode(),
                !empty($e->getResponse())? $e->getResponse()->getBody() : null,
                null,
                !empty($e->getResponse())? $e->getResponse()->getHeaders() : null
            );
        }
    }

    private function decodeResponse($response){
        return json_decode((string) $response->getBody(), true);
    }

}