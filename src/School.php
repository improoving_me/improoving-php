<?php

namespace Improoving;

use Improoving\Helper\Helper;

class School extends ApiResource
{
    const OBJECT_NAME = 'school';

    public $permanent_attributes = array();
    public $related_models = array('association');

    public function __construct($id = null, array $values = array())
    {
        if(!empty($id) && is_array($id))
            parent::__construct($id);
        else{
            $values['id'] = $id;
            parent::__construct($values);
        }
    }

    private static function getDatabaseUrl(){
        return self::getApiUrl().'/school-database';
    }

    public function getTeachers(array $params = array()){
        return $this->_getPeople('teachers', $params);
    }

    public function getStaff(array $params = array()){
        return $this->_getPeople('staff', $params);
    }

    public function getResellers(array $params = array()){
        return $this->_getPeople('resellers', $params);
    }

    public function getCustomers(array $params = array()){
        return $this->_getPeople('customers', $params);
    }

    public function getPackets(array $params = array()){
        return $this->_getPackets('packets', $params);
    }

    public function getEvents(array $params = array()){
        return $this->_getPackets('events', $params);
    }

    public function getMoments(array $params = array()){
        return $this->_getPackets('moments', $params);
    }

    public function getGiftCards(array $params = array()){
        return $this->_getGiftCards($params);
    }

    public function getServices(array $params = array()){
        return $this->_getServices($params);
    }

    public function getCategories(array $params = array()){
        if(!self::bearerTokenIsSet())
            throw new \InvalidArgumentException('You must set a Bearer Token for this call.  HINT: use \Improoving::setBearerToken().');

        $params['OBJECT_NAME'] = Category::OBJECT_NAME;

        $requestor = new ApiRequestor();

        return $requestor->request(
            'get',
            join('/', array(self::getDatabaseUrl(), $this->id, 'categories')),
            $params,
            self::getRequestHeaders()
        );
    }

	public function getPlanner($date, array $params = array()){
		if(!self::bearerTokenIsSet())
			throw new \InvalidArgumentException('You must set a Bearer Token for this call.  HINT: use \Improoving::setBearerToken().');
		else if(!Helper::isValidDate($date))
			throw new \InvalidArgumentException('Wrong date provided.  HINT: use YYYY-MM-DD format.');

		$params['OBJECT_NAME'] = Slot::OBJECT_NAME;

		$requestor = new ApiRequestor();

		return $requestor->request(
			'get',
			join('/', array(self::getDatabaseUrl(), $this->id, 'lessons', $date)),
			$params,
			self::getRequestHeaders()
		);
    }

	public function storeCustomer(Person $customer){
        if(!self::bearerTokenIsSet())
            throw new \InvalidArgumentException('You must set a Bearer Token for this call.  HINT: use \Improoving::setBearerToken().');
        else if(empty($this->id))
            throw new \InvalidArgumentException('School ID not specified.  HINT: pass the ID when building the instance of the School.');
        else if(empty($customer->first_name) || empty($customer->last_name) || empty($customer->email))
            throw new \InvalidArgumentException('Customer must be defined by first_name, last_name and email.  HINT: use \Improoving::Person()->last_name = xxx');
        else if(!$customer->hasConsent(Consent::$default_consent))
            throw new \InvalidArgumentException('Customer must have a default consent set.  HINT: use \Improoving::Person()->giveConsent("data_collection")');

        $params['OBJECT_NAME'] = School::OBJECT_NAME;

        $requestor = new ApiRequestor();

        $customer_array = array();
        foreach($customer->_values as $key => $value)
            $customer_array[$key] = $value;

        $person_array = array(
            array($customer_array)
        );

        return $requestor->request(
            'post',
            join('/', array(self::getApiUrl(), 'school', $this->id, 'people', 'store-many')),
            array_merge($customer->consentsArrayForInput(), ['person' => $person_array]),
            self::getRequestHeaders()
        );
    }

    private function _getPeople($people_group, array $params = array()){
        if(!self::bearerTokenIsSet())
            throw new \InvalidArgumentException('You must set a Bearer Token for this call.  HINT: use \Improoving::setBearerToken().');

        $params['OBJECT_NAME'] = Association::OBJECT_NAME;

        $requestor = new ApiRequestor();

        return $requestor->request(
            'get',
            join('/', array(self::getDatabaseUrl(), $this->id, $people_group)),
            $params,
            self::getRequestHeaders()
        );
    }

    private function _getPackets($packets_group, array $params = array()){
        if(!self::bearerTokenIsSet())
            throw new \InvalidArgumentException('You must set a Bearer Token for this call.  HINT: use \Improoving::setBearerToken().');

        $params['OBJECT_NAME'] = Packet::OBJECT_NAME;

        $requestor = new ApiRequestor();

        return $requestor->request(
            'get',
            join('/', array(self::getDatabaseUrl(), $this->id, $packets_group)),
            $params,
            self::getRequestHeaders()
        );
    }

    private function _getGiftCards(array $params = array()){
        if(!self::bearerTokenIsSet())
            throw new \InvalidArgumentException('You must set a Bearer Token for this call.  HINT: use \Improoving::setBearerToken().');

        $params['OBJECT_NAME'] = GiftCard::OBJECT_NAME;

        $requestor = new ApiRequestor();

        return $requestor->request(
            'get',
            join('/', array(self::getDatabaseUrl(), $this->id, 'giftcards')),
            $params,
            self::getRequestHeaders()
        );
    }

    private function _getServices(array $params = array()){
        if(!self::bearerTokenIsSet())
            throw new \InvalidArgumentException('You must set a Bearer Token for this call.  HINT: use \Improoving::setBearerToken().');

        $params['OBJECT_NAME'] = Service::OBJECT_NAME;

        $requestor = new ApiRequestor();

        return $requestor->request(
            'get',
            join('/', array(self::getDatabaseUrl(), $this->id, 'services')),
            $params,
            self::getRequestHeaders()
        );
    }

}
