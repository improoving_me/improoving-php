<?php

namespace Improoving;

class Account extends ApiResource
{
    const OBJECT_NAME = 'account';

    public $permanent_attributes = array('person_id');
    public $related_models = array('person');

}