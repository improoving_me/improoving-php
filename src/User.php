<?php

namespace Improoving;

class User extends ApiResource
{
    const OBJECT_NAME = 'user';

    public $permanent_attributes = array();
    public $related_models = array('person');

    public function __construct($bearer_token = null, array $values = null)
    {
        if(!empty($bearer_token) && is_array($bearer_token))
            parent::__construct($bearer_token);
        else{
            parent::__construct($values);
            self::$bearer_token = $bearer_token;
        }
    }

    private static function getPersonalUrl(){
        return self::getApiUrl().'/my';
    }

    public function getSchools(array $association_types = null){
        if(!self::bearerTokenIsSet())
            throw new \InvalidArgumentException('You must set a Bearer Token for this call.  HINT: use \Improoving::setBearerToken().');

        $params['OBJECT_NAME'] = School::OBJECT_NAME;
        if(!empty($association_types))
            $params['association_types'] = $association_types;

        $requestor = new ApiRequestor();

        return $requestor->request(
            'get',
            self::getPersonalUrl().'/schools',
            $params,
            self::getRequestHeaders()
        );
    }


}