<?php

namespace Improoving;

class Slot extends ApiResource
{
    const OBJECT_NAME = 'slot';

    public $permanent_attributes = array('id', 'start_time', 'end_time');

}
