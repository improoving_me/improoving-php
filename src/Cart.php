<?php

namespace Improoving;

use Improoving\Error\Cart\CartNotAvailableForPurchase;
use Improoving\Error\Cart\InvalidCart;
use Improoving\Error\Cart\PriceIsWrong;
use Improoving\Error\InvalidParameter;

class Cart extends ImproovingObject
{

    // TODO: write tests

    public $permanent_attributes = array('lessons', 'lessons_no_teacher', 'packets', 'events', 'moments', 'giftcards', 'lessons_services', 'lessons_no_teacher_services', 'packets_services', 'events_services', 'moments_services', 'giftcards_services');
    public $related_models = array();
    
    private static $payer;
    private static $school;
    private static $price = 0;
    private static $charged_from_api = false;
    private static $can_book = false;
    private static $consents;

	private static $categories = array('lessons', 'lessons_no_teacher', 'packets', 'events', 'moments', 'giftcards');
    private static $packet_types = array('packet', 'event', 'moment');
    private static $lesson_types = array('lessons', 'lessons_no_teacher');

    public function __construct(array $values = array()){
        if(!is_array(self::$consents)){
            $consents = Consent::$possible_consents_customers;
            foreach($consents as $consent)
                self::$consents[$consent] = false;
        }

        parent::__construct($this->_buildCartArray($values));
    }

    public function addPacket($packet_id, $pupil_id, $price){
        $this->_addPacket($packet_id, 'packets', $pupil_id, $price);
    }
    public function addEvent($event_id, $pupil_id, $price){
        $this->_addPacket($event_id, 'events', $pupil_id, $price);
    }
    public function addMoment($moment_id, $pupil_id, $price){
        $this->_addPacket($moment_id, 'moments', $pupil_id, $price);
    }
    public function addLessons(array $availability_ids, $category_id, $pupils_number = 1){
        $this->_addLesson($availability_ids, $category_id, $pupils_number);
    }
    public function addLessonsWithoutTeacher(array $slot_ids, $category_id, $pupils_number = 1){
        $this->_addLesson($slot_ids, $category_id, $pupils_number, 'lessons_no_teacher');
    }
    public function addGiftCard($giftcard_id, $quantity = 1){
        if(!isset($this->giftcards[$giftcard_id]))
            $this->giftcards[$giftcard_id] = array(
                'id' => $giftcard_id,
                'selected' => 0
            );

        $this->giftcards[$giftcard_id]['selected'] += (int)$quantity;
    }

    public function addService(int $service_id, string $applies_to, float $price_total){
    	$cart_key = implode('_', [$applies_to, 'services']);
    	if(!in_array($cart_key, $this->permanent_attributes))
    		throw new \InvalidArgumentException('The cart key ['.$cart_key.'] is not valid');

        if(!isset($this->{$cart_key}[$service_id]))
            $this->{$cart_key}[$service_id] = array(
                'id' => $service_id,
                'price_total' => 0,
                'apply_to' => $applies_to
            );

		$this->{$cart_key}[$service_id]['price_total'] += $price_total;
    }

    public function check(){
        if(!self::bearerTokenIsSet())
            throw new \InvalidArgumentException('You must set a Bearer Token for this call.  HINT: use \Improoving::setBearerToken().');

        $this->_checkServiceValidity();

        $requestor = new ApiRequestor();

        $response = $requestor->request(
            'post',
            join('/', array(self::getApiUrl(), 'check-cart')),
            $this->_values,
            self::getRequestHeaders()
        );

        if(!isset($response['cart'])){
            if(is_string($response))
                throw new InvalidCart($response);
            else if(is_array($response) && count($response) == 1)
                throw new InvalidCart($response[0]);
            else
                throw new InvalidCart($response);
        }
        else if(isset($response['cart'])){
            self::$price = $response['price'];
            $this->_values = $this->_buildCartArray($response['cart']);
        }

        if(!$this->isAvailableForPurchase())
            throw new CartNotAvailableForPurchase('The cart is not available for purchase. Hint: change your selection');
        else if(!$this->priceMatches())
            throw new PriceIsWrong('The price you have set is not the same required by the School');

        return $response;
    }
    public function lock(){
        if(!self::bearerTokenIsSet())
            throw new \InvalidArgumentException('You must set a Bearer Token for this call.  HINT: use \Improoving::setBearerToken().');

		$this->_checkServiceValidity();

		$requestor = new ApiRequestor();

        $response = $requestor->request(
            'post',
            join('/', array(self::getApiUrl(), 'checkout')),
            $this->_values,
            self::getRequestHeaders()
        );

        if(!isset($response['cart'])){
            if(is_string($response))
                throw new InvalidCart($response);
            else if(is_array($response) && count($response) == 1)
                throw new InvalidCart($response[0]);
            else
                throw new InvalidCart($response);
        }
        else if(!isset($response['proceed_to_payment']) || !$this->isAvailableForPurchase())
            throw new CartNotAvailableForPurchase('The cart is not available for purchase. HINT: use \Improoving\Cart()->check()');
        else if(isset($response['cart'])){
            self::$price = $response['price'];
            self::$can_book = $response['proceed_to_payment'] == 'true';
            $this->_values = $this->_buildCartArray($response['cart']);
        }

        if(!$this->priceMatches())
            throw new PriceIsWrong('The price you have set is not the same required by the School');

        return $response;
    }
    public function bookAsPayer(){
        if(!self::bearerTokenIsSet())
            throw new \InvalidArgumentException('You must set a Bearer Token for this call.  HINT: use \Improoving::setBearerToken().');
        else if(!self::payerIsSet())
            throw new \InvalidArgumentException('You must specify a Payer for this call.  HINT: use \Improoving\Cart()->useNewPayer() or \Improoving\Cart()->useRegisteredPayer().');
        else if(!self::schoolIsSet())
            throw new \InvalidArgumentException('You must specify a School for this call.  HINT: use \Improoving\Cart::setSchool().');
        else if(!self::$can_book)
            throw new CartNotAvailableForPurchase('You must first check the availability of the products in your cart and lock them.  HINT: use \Improoving\Cart()->lock().');
        else if(!$this->priceMatches())
            throw new PriceIsWrong('The price you have set is not the same required by the School');
        else if(empty(self::$charged_from_api))
            throw new InvalidCart('At the moment, our API does not support charging from our servers; make sure you collect the payment then use \Improoving\Cart::setChargedFromApi(true)');
        else if(empty(self::$consents[Consent::$default_consent]))
            throw new \InvalidArgumentException('Must provide the default consent in order to proceed. HINT: use \Improoving\Cart::giveConsent(data_collection)');

		$this->_checkServiceValidity();

		$requestor = new ApiRequestor();

        $input = array_merge(self::getPayer(), ['charged_from_api' => self::$charged_from_api, 'school-purchases' => json_encode($this->_buildPurchasesArray()), 'cart' => json_encode($this->_values)], $this->_buildConsentsArray());

        $response = $requestor->request(
            'post',
            join('/', array(self::getApiUrl(), 'complete-checkout')),
            $input,
            self::getRequestHeaders()
        );

        return $response;
    }

    public function getPurchase($purchase_id){
        $requestor = new ApiRequestor();
        $response = $requestor->request(
            'get',
            join('/', array(self::getApiUrl(), 'purchases', $purchase_id)),
            array(),
            self::getRequestHeaders()
        );

        return $response;
    }

    public function priceMatches(){
        $assigned_price = 0;

        foreach(self::$categories as $category){
        	if(in_array(substr($category, 0, -1), self::$packet_types)){
				if(!empty($this->{$category}))
					foreach($this->{$category} as $packet)
						foreach($packet['pupils'] as $pupil)
							$assigned_price += (float)$pupil['price'];
			}
        	else if(in_array($category, self::$lesson_types)){
				if(!empty($this->{$category}))
					foreach($this->{$category} as $item)
						if(!empty($item['price']))
							$assigned_price += (float)$item['price'];
			}
        	else{
				if(!empty($this->{$category}))
					foreach($this->{$category} as $item)
						if(!empty($item['price']))
							$assigned_price += (float)$item['price'];
			}

        	// Also services
			$services_key = $category.'_services';
			if(!empty($this->{$services_key}))
				foreach($this->{$services_key} as $service)
					if(!empty($service['price_total']))
						$assigned_price += (float)$service['price_total'];
		}

        return $assigned_price == (float)self::getPrice();
    }

    public function isAvailableForPurchase(){
        $all_available = true;

        try{
            foreach(self::$packet_types as $packet_type)
                if(!empty($this->{$packet_type.'s'}))
                    foreach($this->{$packet_type.'s'} as $item)
                        if(isset($item['available']) && !$item['available'])
                            throw new CartNotAvailableForPurchase('Not Available');

            foreach(self::$lesson_types as $lesson_type)
                if(!empty($this->{$lesson_type}))
                    foreach($this->{$lesson_type} as $item)
                        if(isset($item['available']) && !$item['available'])
                            throw new CartNotAvailableForPurchase('Not Available');

            if(!empty($this->giftcards))
                foreach($this->giftcards as $item)
                    if(isset($item['available']) && !$item['available'])
                        throw new CartNotAvailableForPurchase('Not Available');
        }
        catch(CartNotAvailableForPurchase $e){
            $all_available = false;
        }

        return $all_available;
    }

    public function buildPurchasesArray(){
        return $this->_buildPurchasesArray();
    }

    public static function getSchool(){
        return self::$school;
    }

    public static function getPayer(){
        return self::$payer;
    }

    public static function getConsents(){
        return self::$consents;
    }

    public static function schoolIsSet(){
        return !empty(self::$school);
    }

    public static function payerIsSet(){
        return !empty(self::$payer);
    }

    public static function removePayer(){
        self::$payer = null;
    }

    public static function setSchool(int $school_id){
        self::$school = $school_id;
    }

    public static function setChargedFromApi(bool $status){
        self::$charged_from_api = $status;
    }

    public static function getPrice(){
        return self::$price;
    }

    public function useNewPayer($first_name, $last_name, $email, $phone, $password){
        self::removePayer();
        self::_setPayer($first_name, $last_name, $email, $phone);
        self::$payer['password'] = $password;
        self::$payer['password_confirmation'] = $password;
    }
    public function useRegisteredPayer($person_id, $first_name, $last_name, $email, $phone){
        self::removePayer();
        self::_setPayer($first_name, $last_name, $email, $phone);
        self::$payer['person_id'] = $person_id;
    }

    public function giveConsent($consent_type){
        if(!in_array($consent_type, array_keys(self::$consents)))
            throw new \InvalidArgumentException('Consent does not exist');

        self::$consents[$consent_type] = true;
    }
    public function removeConsent($consent_type){
        if(!in_array($consent_type, array_keys(self::$consents)))
            throw new \InvalidArgumentException('Consent does not exist');

        self::$consents[$consent_type] = false;
    }

    private function _addPacket($packet_id, $packet_category, $pupil_id, $price){
        if(!isset($this->{$packet_category}[$packet_id]))
            $this->{$packet_category}[$packet_id] = array(
                'id' => $packet_id,
                'selected' => 0,
                'pupils' => array()
            );

        $this->{$packet_category}[$packet_id]['selected'] += 1;
        array_push($this->{$packet_category}[$packet_id]['pupils'], array(
            'id' => $pupil_id,
            'price' => $price
        ));
    }

    private function _addLesson(array $item_ids, $category_id, $pupils_number = 1, $lesson_type = 'lessons'){
        if(!in_array($lesson_type, self::$lesson_types))
            throw new \InvalidArgumentException('Lesson type ['.$lesson_type.'] is not valid');

        foreach($item_ids as $item_id){
            $this->{$lesson_type}[$item_id] = array(
                'id' => $item_id,
                'pupils' => $pupils_number,
                'pupils_num' => $pupils_number,
                'category' => array(
                    'id' => $category_id
                )
            );
        }
    }

    private static function _setPayer($first_name, $last_name, $email, $phone){
        self::$payer = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'phone' => $phone
        );
    }

    private function _buildCartArray($values = array()){
        $elaborated_values = array();
        foreach(self::$categories as $category){
            foreach(array('', '_services') as $subject){
                $key = $category.$subject;
                if(isset($values[$key]) && !is_array($values[$key]))
                    throw new InvalidParameter('Cart item ['.$key.'] must be an instance of Array');
                else if(!empty($values[$key]))
                    $elaborated_values[$key] = $values[$key];
                else
                    $elaborated_values[$key] = array();
            }
        }
        return $elaborated_values;
    }

    private function _buildPurchasesArray(){
        $content = array();
        foreach(self::$packet_types as $type){
            foreach($this->{$type.'s'} as $packet_id => $packet_array){
                if(!isset($content[$type.'-to-buy']))
                    $content[$type.'-to-buy'] = array();
                if(!isset($content[$type.'-to-buy'][$packet_id]))
                    $content[$type.'-to-buy'][$packet_id] = array();

                foreach($packet_array['pupils'] as $pupil)
                    array_push($content[$type.'-to-buy'][$packet_id], array(
                        'id' => $packet_id,
                        'paid' => self::$charged_from_api,
                        'price' => $pupil['price'],
                        'pupil' => array(
                            'id' => $pupil['id']
                        )
                    ));
            }

			$this->_buildPurchaseArrayForServicesOfCategory($content, $type.'s');
		}

        foreach(self::$lesson_types as $lesson_type){
            $key = $lesson_type == 'lessons_no_teacher' ? 'slot' : 'ta';
            foreach($this->{$lesson_type} as $item_id => $item_array){
                if(!isset($content[$key.'-to-buy']))
                    $content[$key.'-to-buy'] = array();

                $content[$key.'-to-buy'][$item_array['id']] = array(
                    'id' => $item_array['id'],
                    'pupils_num' => $item_array['pupils'],
                    'pupils_records' => array(),
                    'price' => $item_array['price'], // Make sure this is calculated by platform
                    'paid' => self::$charged_from_api,
                    'activity' => $item_array['activity']
                );
            }

			$this->_buildPurchaseArrayForServicesOfCategory($content, $lesson_type);
		}

        foreach($this->giftcards as $item_id => $item_array){
            if(!isset($content['giftcard-to-buy']))
                $content['giftcard-to-buy'] = array();

            $content['giftcard-to-buy'][$item_array['id']] = array(
                'id' => $item_array['id'],
                'price' => $item_array['price'], // Make sure this is calculated by platform
                'paid' => self::$charged_from_api,
                'selected' => $item_array['selected']
            );
        }

		$this->_buildPurchaseArrayForServicesOfCategory($content, 'giftcards');

		$variable = array(
            self::getSchool() => array(
                'id' => self::getSchool(),
                'content' => $content
            )
        );

        return $variable;
    }

    private function _buildPurchaseArrayForServicesOfCategory(&$content, $category){
		$services_key = $category.'_services';

		if(!isset($this->{$services_key}))
			throw new InvalidParameter('Services for key ['.$services_key.'] are not allowed');

		if(!empty($this->{$services_key}))
			foreach($this->{$services_key} as $service_id => $service){
				if(!isset($content['service-to-buy']))
					$content['service-to-buy'] = array();

				array_push($content['service-to-buy'], array(
					'id' => $service['id'],
					'paid' => self::$charged_from_api,
					'price_total' => $service['price_total'], // This can be set locally
					'apply_to' => $category
				));
			}
	}

    private function _buildConsentsArray(){
        if(!self::schoolIsSet())
            throw new \InvalidArgumentException('You must specify a School before building the consents array.  HINT: use \Improoving\Cart::setSchool().');

        $consents = array(
            'schools' => array(),
            'platform' => array(
                Consent::$default_consent => true
            )
        );
        foreach(self::$consents as $consent => $value)
            $consents['schools'][self::$school][$consent] = $value;

        return $consents;
    }

    private function _checkServiceValidity(){
		foreach(self::$categories as $category)
			if(empty($this->{$category}) && !empty($this->{$category.'_services'}))
				throw new InvalidParameter('Cannot use services for category ['.$category.'] without adding products');

	}
}
