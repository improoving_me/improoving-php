<?php

namespace Improoving;

use Improoving\Error\InvalidParameter;
use Improoving\Helper\Helper;

/**
 * Class ApiRequestor
 *
 * @package Improoving
 */
class ApiRequestor
{

    private static $patterns = [
        'since' => '/^[2][01][0-9]{2}-(0?[1-9]|[1][012])-([012]?[1-9]|[123][01])$/',
        'limit-to' => '/^([1-9][\d|,]+)*([1-9]|[1-9]\d+)$/'
    ];

    // Report required parameters in array
    private static $arrays = [
        'where' => ['key', 'value']
    ];

    /**
     * @var HttpClient\ClientInterface
     */
    private static $_httpClient;

    /**
     * @static
     *
     * @param HttpClient\ClientInterface $client
     */
    public static function setHttpClient($client)
    {
        self::$_httpClient = $client;
    }

    /**
     * @return HttpClient\ClientInterface
     */
    private function httpClient()
    {
        if (!self::$_httpClient) {
            self::$_httpClient = HttpClient\GuzzleClient::instance();
        }
        return self::$_httpClient;
    }

    public function request($method, $url, $params = null, $headers = null)
    {
        if(isset($params['OBJECT_NAME'])){
            $object_name = $params['OBJECT_NAME'];
            unset($params['OBJECT_NAME']);
        }
        $params = $params ?: [];
        $headers = $headers ?: [];

        foreach($params as $key => $value){
            if(in_array($key, array_keys(self::$arrays))){
                if(!is_array($value))
                    throw new InvalidParameter('The argument ['.$key.'] must be an array');

                foreach($value as $array_set){
                    // Check we have all required parameters
                    if(count(self::$arrays[$key]))
                        foreach(self::$arrays[$key] as $required)
                            if(empty($array_set[$required]))
                                throw new InvalidParameter('The argument ['.$key.'] must contain ['.join(', ', self::$arrays[$key]).'] in each instance of the array');

                    foreach($array_set as $key2 => $value2)
                        if(isset(self::$patterns[$key][$key2]) && !preg_match(self::$patterns[$key][$key2], $value2))
                            throw new InvalidParameter('The argument ['.$key2.'] must specify the format ['.self::$patterns[$key][$key2].']');
                }
            }
            else if(isset(self::$patterns[$key]) && !preg_match(self::$patterns[$key], $value))
                throw new InvalidParameter('The argument ['.$key.'] must specify the format ['.self::$patterns[$key].']');
        }

        try{
            $response = $this->httpClient()->request($method, $url, $headers, $params);

            return isset($object_name) ? Helper::convertToImproovingObject($response, $object_name) : $response;
        }
        catch(Error\HttpClient $exception){
            $this->handleErrorResponse(
                $exception->getMessage(),
                $exception->getHttpStatus(),
                $exception->getHttpBody(),
                $exception->getJsonBody(),
                $exception->getHttpHeaders()
            );
        }

    }

    public function handleErrorResponse($message, $code, $httpBody = null, $jsonBody = null, $headers = null)
    {
        // Implement as needed with further updates
        $error = self::_specificOAuthError($message, $code, $httpBody, $jsonBody, $headers);
        if(empty($error))
            $error = self::_specificAPIError($message, $code, $httpBody, $jsonBody, $headers);

        throw $error;
    }

    private static function _specificAPIError($message, $code, $httpBody, $jsonBody, $headers)
    {
        switch ($code) {
            case 400:
                return new Error\InvalidRequest($message, $code, $httpBody, $jsonBody, $headers);
            case 429:
                return new Error\RateLimit($message, $code, $httpBody, $jsonBody, $headers);
            case 401:
                return new Error\Permission($message, $code, $httpBody, $jsonBody, $headers);
            case 404:
                return new Error\InvalidRequest($message, $code, $httpBody, $jsonBody, $headers);
            default:
                return new Error\ApiConnection($message, $code, $httpBody, $jsonBody, $headers);
        }
    }

    private static function _specificOAuthError($message, $code, $httpBody, $jsonBody, $headers)
    {
        $description = isset($jsonBody['error_description']) ? $jsonBody['error_description'] : null;

        switch ($code) {
            case 'invalid_client':
                return new Error\OAuth\InvalidClient($message, $code, $httpBody, $jsonBody, $headers);
            case 'invalid_grant':
                return new Error\OAuth\InvalidGrant($message, $code, $httpBody, $jsonBody, $headers);
            case 'invalid_request':
                return new Error\OAuth\InvalidRequest($message, $code, $httpBody, $jsonBody, $headers);
            case 'invalid_scope':
                return new Error\OAuth\InvalidScope($message, $code, $httpBody, $jsonBody, $headers);
            case 'unsupported_grant_type':
                return new Error\OAuth\UnsupportedGrantType($message, $code, $httpBody, $jsonBody, $headers);
            case 'unsupported_response_type':
                return new Error\OAuth\UnsupportedResponseType($message, $code, $httpBody, $jsonBody, $headers);
        }

        return null;
    }

}
