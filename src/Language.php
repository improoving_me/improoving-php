<?php

namespace Improoving;

use Improoving\ApiOperations\All;

class Language extends ApiResource
{
    const OBJECT_NAME = 'language';

    public $permanent_attributes = array('code', 'name', 'pivot');

    use All;

    private static $allUrl = '/languages';

}
