<?php

namespace Improoving\ApiOperations;

/**
 * Trait for updatable resources. Adds an `update()` static method and a
 * `save()` method to the class.
 *
 * This trait should only be applied to classes that derive from ImproovingObject.
 */
trait Update
{
    public static function update($id, $params = null, $opts = null)
    {
    }

    public function save($opts = null)
    {
    }
}
