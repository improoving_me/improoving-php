<?php

namespace Improoving\ApiOperations;

/**
 * Trait for deletable resources. Adds a `delete()` method to the class.
 *
 * This trait should only be applied to classes that derive from ImproovingObject.
 */
trait Delete
{
    public function delete($params = null, $opts = null)
    {
    }
}
