<?php

namespace Improoving\ApiOperations;

use Improoving\ApiRequestor;

/**
 * Trait for listable resources. Adds a `all()` static method to the class.
 *
 * This trait should only be applied to classes that derive from ImproovingObject.
 */
trait All
{
    public static function getAllUrl(){
        return self::getApiUrl().self::$allUrl;
    }

    public static function all($params = null, $opts = null){
        // Sets what we are requesting, in order to build the object later
        if(!isset($params['OBJECT_NAME']))
            $params['OBJECT_NAME'] = self::OBJECT_NAME;

        $requestor = new ApiRequestor();
        return $requestor->request(
            'get',
            self::getAllUrl(),
            $params
        );
    }
}
