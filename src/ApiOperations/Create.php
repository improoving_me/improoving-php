<?php

namespace Improoving\ApiOperations;

/**
 * Trait for creatable resources. Adds a `create()` static method to the class.
 *
 * This trait should only be applied to classes that derive from ImproovingObject.
 */
trait Create
{
    public static function create($params = null, $options = null)
    {
    }
}
