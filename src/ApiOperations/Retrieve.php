<?php

namespace Improoving\ApiOperations;

/**
 * Trait for retrievable resources. Adds a `retrieve()` static method to the
 * class.
 *
 * This trait should only be applied to classes that derive from ImproovingObject.
 */
trait Retrieve
{
    public static function retrieve($id, $opts = null)
    {
    }
}
