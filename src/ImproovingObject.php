<?php

namespace Improoving;


use Improoving\Helper\Helper;

/**
 * Class ImproovingObject
 *
 * @package Improoving
 */
class ImproovingObject extends Improoving
{
    const NULL_VAL = null;

    protected $_values;

    public function __construct(array $values = null)
    {
        $this->_values = [];
        if($values !== null){
            $check_related = $this->hasRelatedModels();
            foreach($values as $key => $value){
                if($check_related && in_array($key, $this->related_models))
                    $this->_values[$key] = Helper::convertToImproovingObject($value, $key);
                else
                    $this->_values[$key] = $value;
            }
        }
    }

    public function &__get($k)
    {
        // function should return a reference, using NULL_VAL to return a reference to null
        if (!empty($this->_values) && array_key_exists($k, $this->_values)) {
            return $this->_values[$k];
        } else {
            $class = get_class($this);
            Improoving::getLogger()->error("Improoving Notice: Undefined property of $class instance: $k");
            return self::NULL_VAL;
        }
    }

    public function __set($k, $v)
    {
        if (in_array($k, $this->getPermanentAttributes())) {
            throw new \InvalidArgumentException(
                "Cannot set $k on this object. HINT: you can't set: " .
                join(', ', $this->getPermanentAttributes())
            );
        }

        $this->_values[$k] = $v;
    }

    public function __isset($k)
    {
        return isset($this->_values[$k]);
    }

    public function __unset($k)
    {
        unset($this->_values[$k]);
    }

    public static function constructFrom(array $values = null)
    {
        $obj = new static($values);
        return $obj;
    }

    public function getPermanentAttributes()
    {
        // $permanent_attributes must be declared as public array on the Class
        return array_merge(array('id'), !empty($this->permanent_attributes) ? $this->permanent_attributes : []);
    }

    public function hasRelatedModels(){
        return !empty($this->related_models);
    }

    public function flatten($key){
        if($this->__isset($key))
            return array();
        $array = array();
        foreach($this->{$key} as $k => $v)
            $array[$k] = $v;

        return $array;
    }

    public function toArray(){
        $array = [];

        foreach($this->_values as $key => $value)
            $array[$key] = $value;

        return $array;
    }
}
