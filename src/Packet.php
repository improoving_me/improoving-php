<?php

namespace Improoving;

class Packet extends ApiResource
{
    const OBJECT_NAME = 'packet';

    public $permanent_attributes = array('school_id', 'type', 'original_id');
    public $related_models = array('school', 'district', 'levelgroup', 'payers', 'pupils', 'packet_prices', 'variations');

}