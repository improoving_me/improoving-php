<?php

namespace Improoving;

use Improoving\CartFunctions\hasConsents;
use Improoving\CartFunctions\hasPayer;
use Improoving\CartFunctions\hasSchool;
use Improoving\Error\WaitList\InvalidWaitList;
use Improoving\Helper\Helper;

class WaitList extends ImproovingObject
{
    // TODO: write tests

    use hasConsents, hasPayer, hasSchool;

    public $permanent_attributes = array(
        'category_id',
        'district_id',
        'levelgroup_id',
        'age_group',
        'product_id',
        'product_class',
        'school_id',
        'start_date',
        'note',
    );
    public $related_models = array();
    
    private static $price = 0;
    private static $stripe_token = null;
    private static $timeframes = array(
        'morning' => false,
        'afternoon' => false,
        'evening' => false,
    );

    private static $age_groups = array('3-5', '6-9', '10-14', '15-18', '19+');
    private static $levelgroups = array('Novice', 'Beginner', 'Intermediate', 'Expert');

    public function __construct(array $values = array()){
        if(!is_array(self::$consents)){
            $consents = Consent::$possible_consents_customers;
            foreach($consents as $consent)
                self::$consents[$consent] = false;
        }

        parent::__construct($values);
    }

    public function joinWaitList(){
        if(!self::bearerTokenIsSet())
            throw new InvalidWaitList('You must set a Bearer Token for this call.  HINT: use \Improoving::setBearerToken().');
        else if(!self::payerIsSet())
            throw new InvalidWaitList('You must specify a Payer for this call.  HINT: use \Improoving\WaitList()->useNewPayer() or \Improoving\WaitList()->useRegisteredPayer().');
        else if(!self::schoolIsSet())
            throw new InvalidWaitList('You must specify a School for this call.  HINT: use \Improoving\WaitList::setSchool().');
        else if(!$this->_isCompleteWaitList())
            throw new InvalidWaitList('WaitList not defined correctly. HINT use \Improoving\WaitList()->defineWaitList()');
        else if(empty(self::$timeframes['morning'] + self::$timeframes['afternoon'] + self::$timeframes['evening']) && (empty($this->product_id) || empty($this->product_class)))
            throw new InvalidWaitList('You must specify at least a timeframe for the user.  HINT: use \Improoving\WaitList::setTimeFrame().');
        else if(empty(self::$stripe_token))
            throw new InvalidWaitList('You must provide a valid token to be used when the purchase is made; you can pass it using \Improoving\WaitList::setStripeToken()');
        else if(empty(self::$consents[Consent::$default_consent]))
            throw new InvalidWaitList('Must provide the default consent in order to proceed. HINT: use \Improoving\WaitList::giveConsent(data_collection)');

        $requestor = new ApiRequestor();

        $input = array_merge(self::getPayer(), ['stripePaymentIntent' => self::getStripeToken(), 'purchase_type' => 'moments'], $this->_buildWaitListArray(), $this->_buildConsentsArray());

        $response = $requestor->request(
            'post',
            join('/', array(self::getApiUrl(), 'complete-checkout')),
            $input,
            self::getRequestHeaders()
        );

        return $response;
    }

    public function defineWaitList(string $date, int $category_id, int $district_id, string $levelgroup, string $age_group){
        if(!Helper::isValidDate($date))
            throw new \InvalidArgumentException('Incorrect $date. Use format YYYY-MM-DD');
        else if(!in_array($levelgroup, self::$levelgroups))
            throw new \InvalidArgumentException('Incorrect $levelgroup. Can only use ['.implode(', ', self::$levelgroups).']');
        else if(!in_array($age_group, self::$age_groups))
            throw new \InvalidArgumentException('Incorrect $age_group. Can only use ['.implode(', ', self::$age_groups).']');


        parent::__construct(array(
            'start_date' => $date,
            'category_id' => $category_id,
            'district_id' => $district_id,
            'levelgroup_id' => $levelgroup,
            'age_group' => $age_group,
        ));
    }

    public function setTimeFrame($timeframe, bool $allowed = true){
        if(!in_array($timeframe, array_keys(self::$timeframes)))
            throw new \InvalidArgumentException('Incorrect timeframe. Can only use ['.implode(', ', self::$timeframes).']');

        self::$timeframes[$timeframe] = $allowed;
    }

    public function setPrice(float $price){
        self::$price = $price;
    }

    public static function getPrice(){
        return self::$price;
    }

    public function setStripeToken($token){
        self::$stripe_token = $token;
    }

    public function getStripeToken(){
        return self::$stripe_token;
    }

    private function _buildWaitListArray(){
        return array(
            'category_id' => $this->category_id,
            'district_id' => $this->district_id,
            'levelgroup_id' => $this->levelgroup_id,
            'age_group' => $this->age_group,
            // 'product_id' => !empty($this->product_id) ? $this->product_id : null,
            // 'product_class' => !empty($this->product_class) ? $this->product_class : null,
            'school_id' => self::getSchool(),
            'start_date' => $this->start_date,
            'note' => !empty($this->note) ? $this->note : null,
            'timeframe' => $this->_buildTimeFrameInput()
        );
    }

    private function _buildTimeFrameInput(){
        $array = array();
        foreach(self::$timeframes as $timeframe => $selected)
            if($selected)
                array_push($array, $timeframe);

        return $array;
    }

    private function _isCompleteWaitList(){
        return !empty($this->category_id) && !empty($this->district_id) && !empty($this->levelgroup_id) && !empty($this->age_group) && !empty($this->start_date);
    }
}
