<?php

namespace Improoving\Error;

class InvalidRequest extends Base
{
    protected $improovingParam;

    public function __construct(
        $message,
        $improovingParam,
        $httpStatus = null,
        $httpBody = null,
        $jsonBody = null,
        $httpHeaders = null
    ) {
        parent::__construct($message, $httpStatus, $httpBody, $jsonBody, $httpHeaders);
        $this->improovingParam = $improovingParam;
    }

    public function getImproovingParam()
    {
        return $this->improovingParam;
    }
}
