<?php

namespace Improoving\Error\OAuth;

/**
 * InvalidClient is raised when authentication fails.
 */
class InvalidClient extends OAuthBase
{
}
