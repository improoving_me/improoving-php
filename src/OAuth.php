<?php

namespace Improoving;

class OAuth
{
    private static $clientId = null;
    private static $clientSecret = null;

    private static $returnParameters = false;

    public function __construct($client_id, $client_secret = null){
        self::$clientId = $client_id;
        self::$clientSecret = $client_secret;
    }

    public static function getClientId(){
        return self::$clientId;
    }

    public static function getClientSecret(){
        return self::$clientSecret;
    }

    public static function setClientId($client_id){
        self::$clientId = $client_id;
    }

    public static function setClientSecret($client_secret){
        self::$clientSecret = $client_secret;
    }

    public static function setReturnParameters(bool $return_parameters){
        self::$returnParameters = $return_parameters;
    }

    private function checkIfSecretIsSet(){
        if(empty(self::$clientSecret))
            throw new Error\Api('Your client secret is required for this call.');
    }

    private static function oAuthUrl(){
        return str_replace('/api',  '', Improoving::getApiUrl()).'/oauth';
    }

    public function sendToAuthorizationTokenPage($redirect_url){
        $query = http_build_query([
            'client_id' => self::$clientId,
            'redirect_uri' => $redirect_url,
            'response_type' => 'code',
            'scope' => '',
        ]);

        header('Location: '.self::oAuthUrl().'/authorize?'.$query);
        //exit();
    }

    public function getAccessToken($authorization_code, $redirect_url){
        $this->checkIfSecretIsSet();

        $params = array_merge(
            array(
                'grant_type' => 'authorization_code',
                'code' => $authorization_code,
                'redirect_uri' => $redirect_url,
            ),
            $this->_basicRequestParams()
        );
        return $this->_getToken($params);
    }

    public function refreshToken($refresh_token){
        $this->checkIfSecretIsSet();

        $params = array_merge(
            array(
                'grant_type' => 'refresh_token',
                'refresh_token' => $refresh_token,
                'scope' => '',
            ),
            $this->_basicRequestParams()
        );
        return $this->_getToken($params);
    }

    public function getPasswordGrantToken($email, $password){
        $this->checkIfSecretIsSet();

        $params = array_merge(
            array(
                'grant_type' => 'password',
                'username' => $email,
                'password' => $password,
                'scope' => '*',
            ),
            $this->_basicRequestParams()
        );
        return $this->_getToken($params);
    }

    private function _basicRequestParams(){
        return array(
            'client_id' => self::getClientId(),
            'client_secret' => self::getClientSecret(),
        );
    }

    private function _getToken(array $params)
    {
        // When testing we will need only the parameters
        if(self::$returnParameters)
            return $params;

        $requestor = new ApiRequestor();
        return $requestor->request(
            'post',
            self::oAuthUrl().'/token',
            $params
        );
    }
}
