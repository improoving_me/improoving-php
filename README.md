# Improoving.me SDK for PHP Developers

You can sign up for an Improoving.me account at https://improoving.me.

## Requirements

PHP 7.1.0 and later.

## Composer

You can install the bindings via [Composer](http://getcomposer.org/). Run the following command:

```bash
composer require improoving_me/improoving-php
```

To use the bindings, use Composer's [autoload](https://getcomposer.org/doc/01-basic-usage.md#autoloading):

```php
require_once('vendor/autoload.php');
```

## Dependencies

The bindings require the following extensions in order to work properly:

- [`json`](https://secure.php.net/manual/en/book.json.php)
- [`guzzle`](https://github.com/guzzle/guzzle)

If you use Composer, these dependencies should be handled automatically. If you install manually, you'll want to make sure that these extensions are available.

## Getting Started

Simple usage looks like:

```php
# Initialize the School element
$school = new \Improoving\School(array('id' => 2));

# Authorize the user that is making the request
\Improoving\Improoving::setBearerToken($user_token);

# Request the Teachers
$teachers = $school->getTeachers();

echo $teachers;
```

## Documentation

Please see https://test.improoving.me/docs/api for up-to-date documentation.