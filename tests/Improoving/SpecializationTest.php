<?php
namespace Improoving;

class SpecializationTest extends ImproovingTest
{

    public function testValueCannotBeSet(){
        $this->expectException(\InvalidArgumentException::class);

        $specialization = new Specialization();

        $specialization->association_id = 'xxx';
    }
}