<?php
namespace Improoving;

class ConsentTest extends ImproovingTest
{

    public function testValueCannotBeSet(){
        $this->expectException(\InvalidArgumentException::class);

        $location = new Consent();

        $location->consent_type = 'xxx';
    }
}