<?php
namespace Improoving;

class DistrictTest extends ImproovingTest
{

    public function testValueCannotBeSet(){
        $this->expectException(\InvalidArgumentException::class);

        $district = new District();

        $district->approved = 'xxx';
    }
}