<?php
namespace Improoving;

class AccountTest extends ImproovingTest
{

    public function testValueCannotBeSet(){
        $this->expectException(\InvalidArgumentException::class);

        $account = new Account();

        $account->person_id = 'xxx';
    }
}