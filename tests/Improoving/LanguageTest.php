<?php
namespace Improoving;

class LanguageTest extends ImproovingTest
{

    public function testValueCannotBeSet(){
        $this->expectException(\InvalidArgumentException::class);

        $language = new Language();

        $language->name = 'xxx';
    }
}