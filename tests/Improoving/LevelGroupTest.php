<?php
namespace Improoving;

class LevelGroupTest extends ImproovingTest
{

    public function testValueCannotBeSet(){
        $this->expectException(\InvalidArgumentException::class);

        $levelgroup = new LevelGroup();

        $levelgroup->name = 'xxx';
    }
}