<?php
namespace Improoving;

use Improoving\Error\InvalidParameter;

class PacketTest extends ImproovingTest
{

    public function testValueCannotBeSet(){
        $this->expectException(\InvalidArgumentException::class);

        $packet = new Packet();

        $packet->school_id = 'xxx';
    }

    public function testWrongSinceDate(){
        $this->expectException(InvalidParameter::class);
        Improoving::setTestEnvironment(true);
        Improoving::setBearerToken('xxxxx');

        $school = new School(array('id' => 2));
        $school->getPackets(['since' => 'xxx']);
    }

    public function testWrongLimitTo(){
        $this->expectException(InvalidParameter::class);
        Improoving::setTestEnvironment(true);
        Improoving::setBearerToken('xxxxx');

        $school = new School(array('id' => 2));
        $school->getPackets(['limit-to' => 'xxx']);
    }

    public function testWrongWhere(){
        $this->expectException(InvalidParameter::class);
        Improoving::setTestEnvironment(true);
        Improoving::setBearerToken('xxxxx');

        $school = new School(array('id' => 2));
        $school->getPackets(['where' => 'xxx']);
    }
}