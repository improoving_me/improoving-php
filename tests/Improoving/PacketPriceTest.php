<?php
namespace Improoving;

class PacketPriceTest extends ImproovingTest
{

    public function testValueCannotBeSet(){
        $this->expectException(\InvalidArgumentException::class);

        $packet_price = new PacketPrice();

        $packet_price->packet_id = 'xxx';
    }
}