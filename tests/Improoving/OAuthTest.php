<?php
namespace Improoving;

class OAuthTest extends ImproovingTest
{
    /**
     * @var $instance OAuth
     */
    protected $instance = null;

    public function setUp(): void
    {
        parent::setUp();

        $this->instance = new OAuth($this->test_configuration['client_id'], $this->test_configuration['client_secret']);
    }

    public function tearDown(): void
    {
        parent::tearDown();

        $this->instance::setReturnParameters(false);
    }

    public function testParamsForAccessToken(){
        $this->instance::setReturnParameters(true);
        $parameters = $this->instance->getAccessToken('xxx', 'xxx');

        $this->assertEqualsCanonicalizing(array(
            'grant_type', 'client_id', 'client_secret', 'redirect_uri', 'code'
        ), array_keys($parameters));

        $this->assertEquals('authorization_code', $parameters['grant_type']);
    }
    public function testParamsForRefreshToken(){
        $this->instance::setReturnParameters(true);
        $parameters = $this->instance->refreshToken('xxx');

        $this->assertEqualsCanonicalizing(array(
            'grant_type', 'client_id', 'client_secret', 'scope', 'refresh_token'
        ), array_keys($parameters));

        $this->assertEquals('refresh_token', $parameters['grant_type']);
    }
    public function testParamsForPasswordGrantToken(){
        $this->instance::setReturnParameters(true);
        $parameters = $this->instance->getPasswordGrantToken('xxx', 'xxx');

        $this->assertEqualsCanonicalizing(array(
            'grant_type', 'client_id', 'client_secret', 'username', 'password', 'scope'
        ), array_keys($parameters));

        $this->assertEquals('password', $parameters['grant_type']);
        $this->assertTrue(in_array($parameters['scope'], array('', '*')));
    }

}