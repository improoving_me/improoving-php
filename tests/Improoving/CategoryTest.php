<?php
namespace Improoving;

class CategoryTest extends ImproovingTest
{

    public function testValueCannotBeSet(){
        $this->expectException(\InvalidArgumentException::class);

        $category = new Category();

        $category->name = 'xxx';
    }
}