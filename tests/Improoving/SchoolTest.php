<?php
namespace Improoving;

class SchoolTest extends ImproovingTest
{

    public function testHeadersAreSet()
    {
        Improoving::setBearerToken('xxx');

        $school = new School(1);
        $headers = array_keys($school::getRequestHeaders());
        $this->assertTrue(in_array('BearerToken', $headers));
        $this->assertTrue(in_array('Authorization', $headers));

    }

    public function testHeadersAreRequired()
    {
        $this->expectException(\InvalidArgumentException::class);
        $school = new School(1);
        $school->getTeachers();
    }

}