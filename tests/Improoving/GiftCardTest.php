<?php
namespace Improoving;

use Improoving\Error\InvalidParameter;

class GiftCardTest extends ImproovingTest
{

    public function testValueCannotBeSet(){
        $this->expectException(\InvalidArgumentException::class);

        $gift_card = new GiftCard();

        $gift_card->school_id = 'xxx';
    }

    public function testWrongSinceDate(){
        $this->expectException(InvalidParameter::class);
        Improoving::setTestEnvironment(true);
        Improoving::setBearerToken('xxxxx');

        $school = new School(array('id' => 2));
        $school->getGiftCards(['since' => 'xxx']);
    }

    public function testWrongLimitTo(){
        $this->expectException(InvalidParameter::class);
        Improoving::setTestEnvironment(true);
        Improoving::setBearerToken('xxxxx');

        $school = new School(array('id' => 2));
        $school->getGiftCards(['limit-to' => 'xxx']);
    }

    // TODO: check that only items from a specific school are returned
}