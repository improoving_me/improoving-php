<?php
namespace Improoving;

class SlotTest extends ImproovingTest
{

    public function testValueCannotBeSet(){
        $this->expectException(\InvalidArgumentException::class);

        $slot = new Slot();

        $slot->start_time = 'xxx';
    }
}