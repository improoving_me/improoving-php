<?php
namespace Improoving;

class UserTest extends ImproovingTest
{
    /**
     * @var $instance OAuth
     */
    protected $instance = null;

    public function setUp(): void
    {
        parent::setUp();

        $this->instance = new OAuth($this->test_configuration['client_id'], $this->test_configuration['client_secret']);
    }

    public function testHeadersAreSet()
    {
        $user = new User('xxx');
        $headers = array_keys($user::getRequestHeaders());
        $this->assertTrue(in_array('BearerToken', $headers));
        $this->assertTrue(in_array('Authorization', $headers));

    }

    public function testHeadersAreRequired()
    {
        $this->expectException(\InvalidArgumentException::class);
        $user = new User();
        $user->getSchools();
    }

}