<?php
namespace Improoving;

use GuzzleHttp\Psr7\Response;

class ServiceTest extends ImproovingTest
{
    /**
     * @var $instance OAuth
     */
    protected $instance = null;

    public function setUp(): void
    {
        parent::setUp();

        $this->instance = new OAuth($this->test_configuration['client_id'], $this->test_configuration['client_secret']);
    }

    public function testIfServiceIsRunning()
    {
        $this->expectException(Error\InvalidRequest::class);
        $this->instance->getAccessToken('xxx', $this->test_configuration['redirect_uri']);
    }

    public function test400Error(){
        $this->expectException(Error\InvalidRequest::class);
        $this->_mockRequest(400);
    }

    public function test401Error(){
        $this->expectException(Error\Permission::class);
        $this->_mockRequest(401);
    }

    public function test404Error(){
        $this->expectException(Error\InvalidRequest::class);
        $this->_mockRequest(404);
    }

    public function test429Error(){
        $this->expectException(Error\RateLimit::class);
        $this->_mockRequest(429);
    }

    public function testUnclassifiedError(){
        $this->expectException(Error\ApiConnection::class);
        $this->_mockRequest(999);
    }

    private function _mockRequest($error_code){
        $requestor = new ApiRequestor();
        $requestor->handleErrorResponse('xxx', $error_code);
    }

}