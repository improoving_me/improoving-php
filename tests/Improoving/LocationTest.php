<?php
namespace Improoving;

class LocationTest extends ImproovingTest
{

    public function testValueCannotBeSet(){
        $this->expectException(\InvalidArgumentException::class);

        $location = new Location();

        $location->name = 'xxx';
    }
}