<?php
namespace Improoving;

class PersonTest extends ImproovingTest
{

    public function testValueCannotBeSet(){
        $this->expectException(\InvalidArgumentException::class);

        $person = new Person();

        $person->account_id = 'xxx';
    }
}