<?php
namespace Improoving;

use Improoving\Error\InvalidParameter;

class AssociationTest extends ImproovingTest
{

    public function testValueCannotBeSet(){
        $this->expectException(\InvalidArgumentException::class);

        $association = new Association();

        $association->person_id = 'xxx';
    }

    public function testWrongSinceDate(){
        $this->expectException(InvalidParameter::class);
        Improoving::setTestEnvironment(true);
        Improoving::setBearerToken('xxxxx');

        $school = new School(array('id' => 2));
        $school->getTeachers(['since' => 'xxx']);
    }

    public function testWrongLimitTo(){
        $this->expectException(InvalidParameter::class);
        Improoving::setTestEnvironment(true);
        Improoving::setBearerToken('xxxxx');

        $school = new School(array('id' => 2));
        $school->getTeachers(['limit-to' => 'xxx']);
    }

    public function testWrongWhere(){
        $this->expectException(InvalidParameter::class);
        Improoving::setTestEnvironment(true);
        Improoving::setBearerToken('xxxxx');

        $school = new School(array('id' => 2));
        $school->getTeachers(['where' => 'xxx']);
    }
}