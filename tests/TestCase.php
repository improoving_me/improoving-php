<?php

namespace Improoving;

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;

/**
 * Base class for Improoving test cases.
 */
class ImproovingTest extends TestCase
{
    /** @var string original API base URL */
    protected $origApiBase;

    /** @var string original API key */
    protected $origApiKey;

    /** @var string original API version */
    protected $origApiVersion;

    /** @var string original API version */
    protected $origBearerToken;

    /** @var string original account ID */
    protected $origIsTestEnvironment;

    /** @var object HTTP client mocker */
    protected $clientMock;

    protected $test_configuration = array(
        'client_id' => 5,
        'client_secret' => 'EuelytMJCdJ5ow5h9MIHZQ9gQbZWlCCSUrDKeIKH',
        'redirect_uri' => 'http://ski.localhost/admin/authorize-callback'
    );

    protected function setUp(): void
    {
        // Save original values so that we can restore them after running tests
        $this->origApiBase = Improoving::$apiBase;
        $this->origApiKey = Improoving::getApiKey();
        $this->origApiVersion = Improoving::getApiVersion();
        $this->origIsTestEnvironment = Improoving::$isTestEnvironment;
        $this->origBearerToken = Improoving::$bearer_token;

        Improoving::$apiBase = "http://localhost:" . MOCK_PORT;
        Improoving::setTestEnvironment(true);
        Improoving::setApiKey(null);
        Improoving::setApiVersion(null);
        Improoving::setBearerToken(null);
    }

    protected function tearDown(): void
    {
        // Restore original values
        Improoving::$apiBase = $this->origApiBase;
        Improoving::setApiKey($this->origApiKey);
        Improoving::setTestEnvironment($this->origIsTestEnvironment);
        Improoving::setApiVersion($this->origApiVersion);
        Improoving::setBearerToken($this->origBearerToken);
    }

    protected function buildMocker(array $responses){
        $mock = new MockHandler($responses);
        $handler = HandlerStack::create($mock);
        $this->clientMock = new Client(['handler' => $handler]);
    }

}
