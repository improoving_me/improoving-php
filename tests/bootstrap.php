<?php

define("MOCK_MINIMUM_VERSION", "0.33.0");
define("MOCK_PORT", 12111);

include_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/TestCase.php';
